import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {
    static private String html = "<form action=\"login\" method=\"post\">  \n" +
            "Name:<input type=\"text\" name=\"username\"/><br/><br/>  \n" +
            "Password:<input type=\"password\" name=\"pass\"/><br/><br/>  \n" +
            "<input type=\"submit\" value=\"login\"/>  \n" +
            "</form>  ";
    static private String WRONG= "<head><meta charset=\"UTF-8\" content = \"Грешен потребител или парола\"><body><form action=\"login\" method=\"post\">  \n" +
            "Name:<input type=\"text\" name=\"username\"/><br/><br/>  \n" +
            "Password:<input type=\"password\" name=\"pass\"/><br/><br/>  \n" +
            "<input type=\"submit\" value=\"login\"/>  \n" +
            "</form></body></head>  ";
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = "svilen";
        String pass = "svilen123";

        String reqUsername = req.getParameter("username");
        String reqPass = req.getParameter("pass");

        HttpSession session = req.getSession();
        if (reqUsername.equals(user) && reqPass.equals(pass) ) {
            session.setAttribute("currentUser", user);
            resp.sendRedirect(session.getAttribute("redirectUrl").toString());
        } else {
            resp.setContentType("text/html");
            resp.setCharacterEncoding("utf-8");
            resp.getOutputStream().println("Wrong username or password!");
            resp.getOutputStream().println(html);
            //resp.getOutputStream().println(WRONG);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getOutputStream().println(html);
    }
}