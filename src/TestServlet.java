import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;


public class TestServlet extends HttpServlet {
    static private String html = "<html><body><p name = \"hello\">Hello world!</p><img name=kartinka src='/kartinka'></img></body></html>";
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req,resp);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HttpSession session = req.getSession();

        ServletContext context = req.getServletContext();

        String filename = context.getRealPath("/kartinka.jpg");

        String mime = context.getMimeType(filename);
        if (mime==null){
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }

        resp.setContentType(mime);
        File file = new File(filename);
        resp.setContentLength((int) file.length());

        FileInputStream in = new FileInputStream(file);
        OutputStream out = resp.getOutputStream();
        out.write(file.toString().getBytes());

        byte[] buf = new byte[1024];
        int count = 0;
        while ((count=in.read(buf))>=0){
            out.write(buf,0,count);
        }
        resp.getOutputStream().print("name");
        out.close();
        in.close();

    }
}